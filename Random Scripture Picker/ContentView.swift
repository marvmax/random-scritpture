//
//  ContentView.swift
//  Random Scripture Picker
//
//  Created by Max Evans on 5/16/20.
//  Copyright © 2020 Evans Dudes. All rights reserved.
//

import SwiftUI
import UIKit
import Combine

let scriptures = ["Standard Works",
                  "Old Testament",
                  "New Testament",
                  "Book of Mormon",
                  "Doctrine and Conenants",
                  "Pearl of Great Price"]

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(scriptures, id:\.self) { scripture in
            Text(scripture)
                .font(.title)
            }
            .navigationBarTitle("Pick Random Scripture")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
