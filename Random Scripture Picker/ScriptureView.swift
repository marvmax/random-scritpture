//
//  ScriptureView.swift
//  Random Scripture Picker
//
//  Created by Max Evans on 5/25/20.
//  Copyright © 2020 Evans Dudes. All rights reserved.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    let url: URL
    
    func makeUIView(context: UIViewRepresentableContext<WebView>) -> WKWebView {
        let webView = WKWebView()
        
        let request = URLRequest(url: self.url, cachePolicy: .returnCacheDataElseLoad)
        webView.load(request)
        
        return webView
    }
    
    func updateUIView(_ webView: WKWebView, context: UIViewRepresentableContext<WebView>) {
        let request = URLRequest(url: self.url, cachePolicy: .returnCacheDataElseLoad)
        webView.load(request)
    }
}

struct ScriptureView: View {
    var body: some View {
        VStack {
            Text("Random Scripture: Mos 2:1")
                .font(.title)
        WebView(url: URL(string: "https://www.churchofjesuschrist.org/study/scriptures/bofm/mosiah/1.2?lang=eng")!)
        }
    }
}

struct ScriptureView_Previews: PreviewProvider {
    static var previews: some View {
        ScriptureView()
    }
}
